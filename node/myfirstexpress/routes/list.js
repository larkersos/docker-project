var express = require('express');
var router = express.Router();

var querystring = require('querystring');
var url = require("url");

 var bodyParser = require('body-parser');
// create application/json parser
 var jsonParser = bodyParser.json()
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: true })

/* GET users listing. */
router.get('/', function(req, res, next) {
  routerConsole("get",req, res);
  res.send('用户列表页面');
});


/* POST users listing. */
router.post('/',urlencodedParser,function(req, res, next) {
  routerConsole("post",req, res);
  res.send('用户列表页面');
});

function routerConsole(word, req, res) {
  // 输出全局变量 __filename 的值
  console.log( __filename );
  // 输出全局变量 __dirname 的值

  console.log( __dirname );
  console.log("主页 %s 请求req.baseUrl : "+req.baseUrl, word);
  console.log("主页 %s 请求req.hostname : "+req.hostname, word);
  console.log("主页 %s 请求req.originalUrl : "+req.originalUrl, word);
  console.log("主页 %s 请求req.body : "+ JSON.stringify(req.body) || '', word );
  console.log("主页 %s 请求req.path : "+req.path, word);
  console.log("主页 %s 请求req.protocol : "+req.protocol, word);
  console.log("主页 %s 请求req.query : "+  url.parse(req.url).query, word);
  console.log("主页 %s 请求req.query.a : "+ req.query.a, word);
}

module.exports = router;
