var express = require('express');
var router = express.Router();

/* GET redirect */
router.get('/', function(req, res, next) {
  res.redirect('//www.jd.com');
});

module.exports = router;
