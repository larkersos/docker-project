//引入mongodb模块，获得客户端对象
var MongoClient = require('mongodb').MongoClient;
// 连接字符串
// var DB_CONN_STR = 'mongodb://larker1:larker1@101.200.186.113:27017/test';
var DB_CONN_STR = 'mongodb://192.168.0.3:27017/test';
//定义函数表达式，用于操作数据库并返回结果
function insertMongoData(db, callback) {
    //获得指定的集合
    var collection = db.collection('users');
    //插入数据
    var data = [{_id:7,"name":'rose',"age":21},{_id:8,"name":'mark',"age":22}];
    collection.insertOne(data, function(err, result) {
        //如果存在错误
        if(err)
        {
            console.log('Error:'+ err);
            return;
        }
        //调用传入的回调方法，将操作结果返回
        callback(result);
    });
}


function insertData() {
//使用客户端连接数据，并指定完成时的回调方法
    MongoClient.connect(DB_CONN_STR, function(err, db) {
        console.log("连接成功！");
        //执行插入数据操作，调用自定义方法
        insertMongoData(db, function(result) {
            //显示结果
            console.log(result);
            //关闭数据库
            db.close();
        });
    });

}

//定义函数表达式，用于操作数据库并返回结果
function findMongoData(db,param, callback) {
    //获得指定的集合
    var collection = db.collection('users');
    //要查询数据的条件，<=10岁的用户
    var age = param.age;
    console.log('param age:'+ JSON.stringify(param));
    if (!age){
        age = 10;
    }
    var  where={age:{"$eq":parseInt(age)}};
    if (param.name){
        where.name = {"$regex":param.name, $options:'i'} ;
    }
    console.log('where:'+ JSON.stringify(where));
    //要显示的字段
    var set={name:1,age:1};
    collection.find(where,set).toArray(function(err, result) {
        //如果存在错误
        if(err)
        {
            console.log('Error:'+ err);
            callback(err);
            return;
        }
        //调用传入的回调方法，将操作结果返回
        callback(result);
    });
}

var dataContent = "";
function findData(param,callback) {
    dataContent = "这是一个mongo数据查询";
    //使用客户端连接数据，并指定完成时的回调方法
    MongoClient.connect(DB_CONN_STR, param, function(err, db) {
        //如果存在错误
        if(err)
        {
            console.log('Error:'+ err);
            callback(err);
            return;
        }
        console.log("连接成功！");
        //执行插入数据操作，调用自定义方法
        findMongoData(db,param, callback);
    });

    return dataContent;
}

// 定义函数表达式，用于操作数据库并返回结果
function deleteMongoData (db, callback) {
    //获得指定的集合
    var collection = db.collection('users');
    //要删除数据的条件，_id>2的用户删除
    var  where={_id:{"$gt":2}};
    collection.deleteOne(where,function(err, result) {
        //如果存在错误
        if(err)
        {
            console.log('Error:'+ err);
            return;
        }
        //调用传入的回调方法，将操作结果返回
        callback(result);
    });
}
function deleteData() {
    //使用客户端连接数据，并指定完成时的回调方法
    MongoClient.connect(DB_CONN_STR, function(err, db) {
        console.log("连接成功！");
        //执行插入数据操作，调用自定义方法
        deleteMongoData(db, function(result) {
            //显示结果
            console.log(result);
            //关闭数据库
            db.close();
        });
    });

}

// 对外
exports.insertData = insertData;
exports.findData = findData;
exports.deleteData = deleteData;
